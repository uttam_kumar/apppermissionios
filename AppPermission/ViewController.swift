//
//  ViewController.swift
//  AppPermission
//
//  Created by Syncrhonous on 20/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func checkPermission(_ sender: UIButton) {
        
        if checkMicrophoneAndCameraPermissionGranted(){
            label.text = "granted"
        }else{
            presentPhoneSettings()
        }
    }
    
    //check camera and microphone is granted
    func checkMicrophoneAndCameraPermissionGranted() -> Bool{
        if(checkCameraAccess() && checkMicrophoneAccess()){
            return true
        }
        return false
    }
    
    //microphone access permission
    func checkMicrophoneAccess() -> Bool {
        var  isparmited = false
        
        switch AVAudioSession.sharedInstance().recordPermission  {
        case .denied:
            print("Denied, request permission from settings")
            presentPhoneSettings()
        //label.text = "Denied, request permission from settings"
        case .granted:
            print("Authorized, proceed")
            //label.text = "Authorized, proceed"
            isparmited = true
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission { success in
                if success {
                    //self.label.text = "Permission granted, proceed"
                    print("Permission granted, proceed")
                    isparmited = true
                } else {
                    //self.label.text = "Permission, proceed"
                    print("Permission denied")
                    isparmited = false
                }
            }
        }
        return isparmited
    }
    
    
    //camera access permission
    func checkCameraAccess() -> Bool {
        var  isparmited = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print("Denied, request permission from settings")
            presentPhoneSettings()
        //label.text = "Denied, request permission from settings"
        case .restricted:
            //label.text = "Restricted, device owner must approve"
            print("Restricted, device owner must approve")
            isparmited = false
        case .authorized:
            print("Authorized, proceed")
            //label.text = "Authorized, proceed"
            isparmited = true
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    //self.label.text = "Permission granted, proceed"
                    print("Permission granted, proceed")
                    isparmited = true
                } else {
                    //self.label.text = "Permission, proceed"
                    print("Permission denied")
                    isparmited = false
                }
            }
        }
        return isparmited
    }
    
    //phone settings display
    func presentPhoneSettings() {
        let alertController = UIAlertController(title: "App Permission Required", message: "Camera and Microphone permission are highly required for Synopi Live 360.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                        // Handle
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        })
        
        present(alertController, animated: true)
    }
}

